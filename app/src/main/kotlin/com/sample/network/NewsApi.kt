package com.sample.network

import io.reactivex.Observable
import retrofit2.http.GET

/**
 * The interface which provides methods to get result of webservices
 */
interface NewsApi {
    @GET("/v2/top-headlines?country=us&pageSize=100")
    fun getHeadLines(): Observable<NewsResponseJson>


}