package com.sample.network

import com.sample.dto.NewsArticleEntity

data class ArticleResponseJson(
    var author: String?,
    var title: String?,
    var description: String?,
    var url: String?,
    var urlToImage: String?,
    var publishedAt: String?,
    var content: String?
) {
    fun convert(): NewsArticleEntity = NewsArticleEntity(
        author ?: "",
        title ?: "",
        description ?: "",
        url ?: "",
        urlToImage ?: "",
        publishedAt ?: "",
        content ?: "",
        System.currentTimeMillis()
    )
}