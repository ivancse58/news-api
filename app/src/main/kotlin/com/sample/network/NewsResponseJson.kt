package com.sample.network

import com.sample.dto.NewsArticleEntity

data class NewsResponseJson(
    var status: String?,
    val totalResults: Long?,
    var articles: List<ArticleResponseJson>?
) {

    fun convertToEntity(): List<NewsArticleEntity> {
        return articles?.map { item -> item.convert() }.orEmpty()
    }
}