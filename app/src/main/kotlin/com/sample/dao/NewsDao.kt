package com.sample.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.sample.dto.NewsArticleEntity


@Dao
interface NewsDao {
    @get:Query("SELECT * FROM newsarticleentity")
    val all: List<NewsArticleEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg users: NewsArticleEntity)

    @Query("DELETE FROM newsarticleentity")
    fun deleteAll()

    @get:Query("SELECT MAX(lastUpdateTime) FROM newsarticleentity")
    val lastUpdateTime: Long
}