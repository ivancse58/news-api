package com.sample.dao

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.sample.dto.NewsArticleEntity

@Database(entities = [NewsArticleEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun newsDao(): NewsDao
}