package com.sample.utils

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Observer
import android.databinding.BindingAdapter
import android.support.v4.content.ContextCompat
import android.support.v4.widget.CircularProgressDrawable
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import com.sample.R
import com.sample.utils.extension.getParentActivity
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale


@BindingAdapter("adapter")
fun setAdapter(view: RecyclerView, adapter: RecyclerView.Adapter<*>) {
    view.adapter = adapter
}

@BindingAdapter("mutableVisibility")
fun setMutableVisibility(view: View, visibility: MutableLiveData<Int>?) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()
    if (parentActivity != null && visibility != null) {
        visibility.observe(
            parentActivity,
            Observer { value -> view.visibility = value ?: View.VISIBLE })
    }
}

@BindingAdapter("mutableText")
fun setMutableText(view: TextView, text: MutableLiveData<String>?) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()
    if (parentActivity != null && text != null) {
        text.observe(parentActivity, Observer { value -> view.text = value ?: "" })
    }
}

/*
*Added CircularProgressDrawable as placeholder to show loading state of the banner image
 */
@BindingAdapter("imageUrl")
fun setImageUrl(imageView: ImageView, url: String?) {
    if (url == null) return
    val context = imageView.context
    val drawable = CircularProgressDrawable(context)
    drawable.setColorSchemeColors(ContextCompat.getColor(context, R.color.colorAccent));
    drawable.centerRadius = 40.0f;
    drawable.strokeWidth = 10.0f;
    drawable.start()
    Glide.with(context).load(url)
        .apply(RequestOptions().override(Target.SIZE_ORIGINAL, 540).placeholder(drawable))
        .into(imageView)
}

@BindingAdapter("publishDate")
fun setPublishDate(view: TextView, text: MutableLiveData<String>?) {

    val parentActivity: AppCompatActivity? = view.getParentActivity()
    if (parentActivity != null && text != null) {
        text.observe(parentActivity, Observer { value ->
            view.text = parseListDate(value)
        })
    }
}
/*
*Handle server side date format
 */
fun parseListDate(publishDate: String?): String {
    val dateFormat = SimpleDateFormat("dd MMMM yyyy, hh:mm", Locale.US)
    val date: Date = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", Locale.US).parse(publishDate)
    return dateFormat.format(date.time)
}