package com.sample.news.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.sample.R
import com.sample.databinding.NewsItemBinding
import com.sample.dto.NewsArticleEntity
import com.sample.news.viewmodel.NewsViewModel

class NewsListAdapter : RecyclerView.Adapter<NewsListAdapter.ViewHolder>() {
    private lateinit var itemList: List<NewsArticleEntity>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: NewsItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.news_item,
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(itemList[position])
    }

    override fun getItemCount(): Int {
        return if (::itemList.isInitialized) itemList.size else 0
    }

    fun updateItemList(itemList: List<NewsArticleEntity>) {
        this.itemList = itemList
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: NewsItemBinding) : RecyclerView.ViewHolder(binding.root) {
        private val viewModel = NewsViewModel()

        fun bind(newsArticleEntity: NewsArticleEntity) {
            viewModel.bind(newsArticleEntity)
            binding.viewModel = viewModel
        }
    }
}