package com.sample.news.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.util.Log
import android.view.View
import com.sample.R
import com.sample.base.BaseViewModel
import com.sample.dao.NewsDao
import com.sample.dto.NewsArticleEntity
import com.sample.network.NewsApi
import com.sample.news.adapter.NewsListAdapter
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


private const val REFRESH_INTERVAL = 5 * 60 * 1000L // 5 min

class NewsListViewModel(private val newsDao: NewsDao) : BaseViewModel() {
    @Inject
    lateinit var newsApi: NewsApi
    val newsListAdapter: NewsListAdapter =
            NewsListAdapter()

    val errorVisibility: MutableLiveData<Int> = MutableLiveData()
    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val errorMessage: MutableLiveData<Int> = MutableLiveData()
    val errorClickListener = View.OnClickListener { loadItems() }

    private lateinit var subscription: Disposable

    init {
        loadItems()
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }

    /*
    *Data will update if last update before 5 min
     */
    private fun loadItems() {
        subscription = Observable.fromCallable { newsDao.all }
                .concatMap {
                    val lastUpdateTime = newsDao.lastUpdateTime
                    if (it.isEmpty()) {
                        Log.d("NewsListViewModel", "Data get form api")
                        newsApi.getHeadLines().concatMap {
                            val items = it.convertToEntity()
                            newsDao.deleteAll()
                            newsDao.insertAll(*items.toTypedArray())
                            Observable.just(items)
                        }
                    } else {
                        if (System.currentTimeMillis() - lastUpdateTime > REFRESH_INTERVAL) {
                            Log.d("NewsListViewModel", "Data Update form api")
                            newsApi.getHeadLines().map {
                                val items = it.convertToEntity()
                                newsDao.deleteAll()
                                newsDao.insertAll(*items.toTypedArray())
                            }.subscribe().dispose()
                        }
                        Observable.just(it)
                    }

                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { onRetrieveStart() }
                .doOnTerminate { onRetrieveFinish() }
                .subscribe(
                        { result -> onRetrieveSuccess(result) },
                        { onRetrieveError() }
                )
    }

    private fun onRetrieveStart() {
        errorVisibility.value = View.INVISIBLE
        loadingVisibility.value = View.VISIBLE
        errorMessage.value = null
    }

    private fun onRetrieveFinish() {
        loadingVisibility.value = View.GONE
    }

    private fun onRetrieveSuccess(itemList: List<NewsArticleEntity>) {
        newsListAdapter.updateItemList(itemList)
    }

    private fun onRetrieveError() {
        errorVisibility.value = View.VISIBLE
        errorMessage.value = R.string.error
    }
}