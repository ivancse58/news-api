package com.sample.news.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.content.Intent
import android.net.Uri
import android.view.View
import com.sample.base.BaseViewModel
import com.sample.dto.NewsArticleEntity


class NewsViewModel : BaseViewModel() {
    private val publishDate = MutableLiveData<String>()
    private val itemTitle = MutableLiveData<String>()
    private val itemBody = MutableLiveData<String>()
    private val itemImageUrl = MutableLiveData<String>()
    private val itemUrl = MutableLiveData<String>()

    fun bind(item: NewsArticleEntity) {
        itemTitle.value = item.title
        itemBody.value = item.description
        itemImageUrl.value = item.urlToImage
        publishDate.value = item.publishedAt
        itemUrl.value = item.url
    }

    fun itemOnClick(view: View) {
        val uris = Uri.parse(itemUrl.value)
        val intents = Intent(Intent.ACTION_VIEW, uris)
        view.context.startActivity(intents)
    }

    fun getPublishDate(): MutableLiveData<String> {
        return publishDate
    }

    fun getImageUrl(): MutableLiveData<String> {
        return itemImageUrl
    }

    fun getTitle(): MutableLiveData<String> {
        return itemTitle
    }

    fun getDescription(): MutableLiveData<String> {
        return itemBody
    }
}