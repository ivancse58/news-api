package com.sample.injection.component

import com.sample.injection.module.NetworkModule
import com.sample.news.viewmodel.NewsListViewModel
import com.sample.news.viewmodel.NewsViewModel
import dagger.Component
import javax.inject.Singleton

/**
 * Component providing inject() methods for presenters.
 */
@Singleton
@Component(modules = [(NetworkModule::class)])
interface ViewModelInjector {
    /**
     * Injects required dependencies into the specified NewsListViewModel.
     * @param newsListViewModel NewsListViewModel in which to inject the dependencies
     */
    fun inject(newsListViewModel: NewsListViewModel)

    /**
     * Injects required dependencies into the specified NewsViewModel.
     * @param newsViewModel NewsViewModel in which to inject the dependencies
     */
    fun inject(newsViewModel: NewsViewModel)

    @Component.Builder
    interface Builder {
        fun build(): ViewModelInjector

        fun networkModule(networkModule: NetworkModule): Builder
    }
}