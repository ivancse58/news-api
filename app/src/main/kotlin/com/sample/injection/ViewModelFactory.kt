package com.sample.injection

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.persistence.room.Room
import android.support.v7.app.AppCompatActivity
import com.sample.dao.AppDatabase
import com.sample.news.viewmodel.NewsListViewModel

class ViewModelFactory(private val activity: AppCompatActivity) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(NewsListViewModel::class.java)) {
            val db =
                Room.databaseBuilder(activity.applicationContext, AppDatabase::class.java, "news")
                    .build()
            @Suppress("UNCHECKED_CAST")
            return NewsListViewModel(db.newsDao()) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")

    }
}