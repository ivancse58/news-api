package com.sample.base

import android.arch.lifecycle.ViewModel
import com.sample.injection.component.DaggerViewModelInjector
import com.sample.injection.component.ViewModelInjector
import com.sample.injection.module.NetworkModule
import com.sample.news.viewmodel.NewsListViewModel
import com.sample.news.viewmodel.NewsViewModel

abstract class BaseViewModel : ViewModel() {
    private val injector: ViewModelInjector = DaggerViewModelInjector
        .builder()
        .networkModule(NetworkModule)
        .build()

    init {
        inject()
    }

    /**
     * Injects the required dependencies
     */
    private fun inject() {
        when (this) {
            is NewsListViewModel -> injector.inject(this)
            is NewsViewModel -> injector.inject(this)
        }
    }
}